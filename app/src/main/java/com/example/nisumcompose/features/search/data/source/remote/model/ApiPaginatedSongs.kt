package com.example.nisumcompose.features.search.data.source.remote.model

import com.google.gson.annotations.SerializedName

data class ApiPaginatedSongs(
    val info: ApiSongs?,
    @SerializedName("results") val songs: List<ApiSongs>
)
