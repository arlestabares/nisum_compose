package com.example.nisumcompose.features.search.domain.model.songs

data class SongList(
    val resultCount: Int,
    val results: List<Song>
)