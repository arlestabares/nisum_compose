package com.example.nisumcompose.features.search.data.source.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.nisumcompose.features.search.data.source.local.daos.SongDao
import com.example.nisumcompose.features.search.data.source.local.models.CachedSongs


@Database(entities = [CachedSongs::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun songDao(): SongDao
}