package com.example.nisumcompose.features.search.domain.model.songs

data class Song(
    val trackId: Int,
    val artworkUrl100: String,
    val collectionName: String,
    val artistName: String,
    val collectionViewUrl: String,
    val previewUrl: String,
    val trackName: String,
    val trackViewUrl: String,

)