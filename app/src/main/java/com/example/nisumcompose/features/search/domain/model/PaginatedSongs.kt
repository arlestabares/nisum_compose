package com.example.nisumcompose.features.search.domain.model

import com.example.nisumcompose.features.search.domain.model.info.Info
import com.example.nisumcompose.features.search.domain.model.songs.SongList

data class PaginatedSongs(val info: Info, val songs: List<SongList>)
