package com.example.nisumcompose.features.search.di

import com.example.nisumcompose.features.search.data.repositories.SongRepositoryImpl
import com.example.nisumcompose.features.search.domain.repositories.SongDomainRepository
import dagger.Binds
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Singleton
@InstallIn(SingletonComponent::class)
 abstract  class RepositoryModule {
     @Binds
     abstract  fun bindSongRepository(impl: SongRepositoryImpl): SongDomainRepository
}