package com.example.nisumcompose.features.search.presentation.home.components.songs_list

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.nisumcompose.R
import com.example.nisumcompose.features.search.domain.model.songs.Song
import com.example.nisumcompose.features.search.presentation.navigation.Destinations
import com.example.nisumcompose.features.search.presentation.viewmodel.SongListViewModel

@Composable
fun SongsListScreen(
    navController: NavController,
    viewModel: SongListViewModel = hiltViewModel()
) {
//    val songList by viewModel.getSongsList().observeAsState()
    SongsScreen(navController, listOf())

}

@Composable
fun SongsScreen(
    navController: NavController,
    songs: List<Song>
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("ITunes Song") },
            )
        }
    ) {
        LazyColumn {
            items(songs) { song ->
                Card(
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier.padding(8.dp).fillParentMaxWidth().clickable {
                        //TODO: Navegar a detalle
                        navController.currentBackStackEntry?.savedStateHandle?.set("song", song)
                        navController.navigate(
                            Destinations.songDetailsArgumentsScreen.passId(
                                song.trackId,
                                song.collectionViewUrl
                            )
                        )
                    }
                ) {
                    Column {
                        Image(
                            modifier = Modifier.fillParentMaxWidth()
                                .aspectRatio(15f / 12f),
                            painter = rememberImagePainter(
                                data = song.collectionViewUrl,
                                builder = {
                                    placeholder(R.drawable.no_image)
                                    error(R.drawable.no_image)
                                }
                            ),
                            contentDescription = null,
                            contentScale = ContentScale.FillWidth
                        )
                        Column(Modifier.padding(8.dp)) {
                            Text(
                                song.artistName,
//                                fontSize = 18.dp,
//                                fontWeight = FontWeight.Bold
                            )

                        }
                    }
                }
            }
        }

    }

}
