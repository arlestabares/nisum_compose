package com.example.nisumcompose.features.search.presentation.navigation

const val DETAIL_ARGUMENT_KEY = "id"
const val DETAIL_TITLE_ARGUMENT_KEY = "title"
const val DETAIL_OVERVIEW_ARGUMENT_KEY = "overview"
const val COLLECTION_VIEW_URL = "collectionViewUrl"

sealed class Destinations(
    val route: String
) {
    object songListScreen : Destinations("SONG_LIST_SCREEN")
    object songDetailsScreen : Destinations("SONG_DETAILS_SCREEN")
    object songDetailsArgumentsScreen :
        Destinations("SONG_DETAILS_SCREEN/{$DETAIL_ARGUMENT_KEY}/{$COLLECTION_VIEW_URL}") {
        fun passId(id: Int, collectionViewUrl: String): String {
            return "SONG_DETAILS_SCREEN/$id/$collectionViewUrl"
        }
    }

}
