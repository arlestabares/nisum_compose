package com.example.nisumcompose.features.search.domain.use_case

import com.example.nisumcompose.features.search.domain.model.songs.SongList
import com.example.nisumcompose.features.search.domain.repositories.SongDomainRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import javax.inject.Inject

class GetSongsUseCase @Inject constructor(
    private val songDomainRepository: SongDomainRepository
) {
    operator fun invoke(name: String, limit: Int): Flow<List<SongList>> {
        return songDomainRepository.getSongs(name, limit).filter { it.isNotEmpty() }
    }
}