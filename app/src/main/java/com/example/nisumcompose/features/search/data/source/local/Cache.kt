package com.example.nisumcompose.features.search.data.source.local

import com.example.nisumcompose.features.search.data.source.local.models.CachedSongs
import kotlinx.coroutines.flow.Flow

interface Cache {

    fun getSongs(input: String, limit: Int): Flow<List<CachedSongs>>

    suspend fun storeSongs(songs: List<CachedSongs>)
}