package com.example.nisumcompose.features.search.data.source.remote.network

import com.example.nisumcompose.features.search.data.source.remote.model.ApiPaginatedSongs
import retrofit2.http.GET
import retrofit2.http.Query


const val SONG_ENDPOINT = "search?term="
const val TERM = "term"
const val MEDIA_TYPE = "mediaType"
const val LIMIT = " limit"

interface SongApi {

    @GET(SONG_ENDPOINT)
    suspend fun getSongs(
        @Query(TERM) term: String,
        @Query(MEDIA_TYPE) mediaType: String,
        @Query(LIMIT) limit: Int
    ): ApiPaginatedSongs
}