package com.example.nisumcompose.features.search.data.source.local

import com.example.nisumcompose.features.search.data.source.local.daos.SongDao
import com.example.nisumcompose.features.search.data.source.local.models.CachedSongs
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RoomCache @Inject constructor(
    private val songDao: SongDao
) : Cache {
    override fun getSongs(trackName: String, limit: Int): Flow<List<CachedSongs>> {
        return songDao.getSongs(trackName, limit)
    }

    override suspend fun storeSongs(songs: List<CachedSongs>) {
        return songDao.insertSong(songs)
    }

}