package com.example.nisumcompose.features.search.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.nisumcompose.features.search.domain.model.songs.Song
import com.example.nisumcompose.features.search.presentation.home.components.song_details.SongDetailsScreen
import com.example.nisumcompose.features.search.presentation.home.components.songs_list.SongsListScreen

@Composable
fun NavigationHost() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Destinations.songListScreen.route) {
        composable(Destinations.songListScreen.route) {
            SongsListScreen(navController)
        }
        composable(route = Destinations.songDetailsScreen.route) {
            val song = navController.previousBackStackEntry?.savedStateHandle?.get<Song>("song")
            if (song != null) {
                SongDetailsScreen(song = song, navController)
            }
        }
    }
}