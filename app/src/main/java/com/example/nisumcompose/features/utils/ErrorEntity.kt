package com.example.nisumcompose.features.utils

sealed class ErrorEntity {
    sealed class ApiError : ErrorEntity() {
        object NotFound : ApiError()
        object UnKnowm : ApiError()
    }

    sealed class InputError : ErrorEntity() {
        object NameError : InputError()
    }
}