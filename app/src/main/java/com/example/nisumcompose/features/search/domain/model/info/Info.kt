package com.example.nisumcompose.features.search.domain.model.info

data class Info(
    val resultCount: Int,//numero de items
) {
    companion object {
        const val DEFAULT_COUNT_SONG = 20
    }
}
