package com.example.nisumcompose.features.search.data.source.remote.model

data class ApiSongs(
    val trackId: Int,
    val artworkUrl100: String,
    val collectionName: String,
    val artistName: String,
    val collectionViewUrl: String,
    val previewUrl: String,
    val trackName: String,
    val trackViewUrl: String,
)
