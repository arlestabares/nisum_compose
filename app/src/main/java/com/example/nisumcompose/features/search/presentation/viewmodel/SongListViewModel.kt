package com.example.nisumcompose.features.search.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nisumcompose.features.search.domain.model.songs.Song
import com.example.nisumcompose.features.search.domain.use_case.GetSongsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SongListViewModel @Inject constructor(
    private val getSongsUseCase: GetSongsUseCase,
): ViewModel(){

    private val _songList= MutableLiveData<List<Song>>()
    val songsLiveData:LiveData<List<Song>> = _songList

    private val _song = MutableLiveData<Song>()
    var songLiveData:LiveData<Song> = _song


//     fun getSongsList():LiveData<List<Song>>{
//         viewModelScope.launch(Dispatchers.IO) {
//             val song = getSongsUseCase.invoke()
//             _songList.postValue(song)
//         }
//     }

}