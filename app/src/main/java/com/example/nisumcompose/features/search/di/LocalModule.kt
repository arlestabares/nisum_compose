package com.example.nisumcompose.features.search.di

import android.app.Application
import androidx.room.Room
import com.example.nisumcompose.features.search.data.source.local.Cache
import com.example.nisumcompose.features.search.data.source.local.RoomCache
import com.example.nisumcompose.features.search.data.source.local.daos.SongDao
import com.example.nisumcompose.features.search.data.source.local.db.AppDatabase
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


const val DB_SONG = "app_database"

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalModule {
    @Binds
    abstract fun bindCache(cache: RoomCache): Cache

    @Provides
    fun provideSongDao(db: AppDatabase): SongDao = db.songDao()

    companion object {
        @Singleton
        @Provides
        fun provideDatabase(app: Application): AppDatabase {
            return Room.databaseBuilder(
                app,
                AppDatabase::class.java,
                DB_SONG
            ).build()
        }
    }

}