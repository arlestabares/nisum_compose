package com.example.nisumcompose.features.search.data.source.local.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.nisumcompose.features.search.data.source.local.models.CachedSongs
import kotlinx.coroutines.flow.Flow


@Dao
interface SongDao {

    @Query("SELECT * FROM  songs WHERE trackName LIKE '%' || :trackName ||  '%' LIMIT :limit ")
    fun getSongs(trackName: String, limit: Int): Flow<List<CachedSongs>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSong(song: List<CachedSongs>)
}