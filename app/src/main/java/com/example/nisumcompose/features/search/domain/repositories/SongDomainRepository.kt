package com.example.nisumcompose.features.search.domain.repositories

import com.example.nisumcompose.features.search.domain.model.info.Info
import com.example.nisumcompose.features.search.domain.model.songs.SongList
import kotlinx.coroutines.flow.Flow

interface SongDomainRepository {
    fun getSongs(input: String, limit: Int): Flow<List<SongList>>

    suspend fun requestMoreSongs(name: String, limit: Int): Result<Info>
}