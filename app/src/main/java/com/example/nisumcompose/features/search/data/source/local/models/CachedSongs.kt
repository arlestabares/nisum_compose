package com.example.nisumcompose.features.search.data.source.local.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.nisumcompose.features.search.domain.model.songs.Song
import com.example.nisumcompose.features.search.domain.model.songs.SongList

@Entity(tableName = "songs")
data class CachedSongs(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val artistName: String,
    val artworkUrl100: String,
    val collectionName: String,
    val collectionViewUrl: String,
    val previewUrl: String,
    val trackId: Int,
    val trackName: String,
    val trackViewUrl: String,

    ) {
    companion object {
        fun fromDomain(domainModel: Song): CachedSongs {
            return CachedSongs(
                id = domainModel.trackId,
                artistName = domainModel.artistName,
                artworkUrl100 = domainModel.artworkUrl100,
                collectionName = domainModel.collectionName,
                collectionViewUrl = domainModel.collectionViewUrl,
                previewUrl = domainModel.previewUrl,
                trackId = domainModel.trackId,
                trackName = domainModel.trackName,
                trackViewUrl = domainModel.trackViewUrl,
            )
        }
    }

    fun toDomain(): Song {
        return Song(
            trackId = id,
            artistName = artistName,
            artworkUrl100 = artworkUrl100,
            collectionName = collectionName,
            collectionViewUrl = collectionViewUrl,
            previewUrl = previewUrl,
            trackName = trackName,
            trackViewUrl = trackViewUrl,

            )
    }
}