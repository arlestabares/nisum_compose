package com.example.nisumcompose

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SongSearchApplication :Application() {
}